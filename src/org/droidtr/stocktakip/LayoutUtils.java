package org.droidtr.stocktakip;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import static org.droidtr.stocktakip.ColorUtils.TEAL_LIGHT;
import static org.droidtr.stocktakip.ColorUtils.WHITE;
import static org.droidtr.stocktakip.DensityUtils.dp;
import static org.droidtr.stocktakip.DensityUtils.dpInt;

public class LayoutUtils {
	
	public static final LinearLayout HBox(Context ctx,boolean i){
		LinearLayout ll = new LinearLayout(ctx);
		if(i){
			int p = DensityUtils.dpInt(8);
			ll.setPadding(p,p,p,p);
		}
		return ll;
	}
	
	public static final LinearLayout VBox(Context ctx){
		return VBox(ctx,true);
	}
	
	public static final LinearLayout HBox(Context ctx){
		return HBox(ctx,true);
	}
	
	public static final LinearLayout VBox(Context ctx,boolean i){
		LinearLayout ll = HBox(ctx,i);
		ll.setOrientation(LinearLayout.VERTICAL);
		return ll;
	}
	
	public static final ActionBar ActionBar(Activity ctx){
		ActionBar ab = new ActionBar(ctx);
		ab.getLabelView().setText(ctx.getTitle());
		ab.addIcon(ctx.getResources().getDrawable(R.drawable.icon));
		return ab;
	}
	
	public static class ActionBar extends LinearLayout {
		
		private TextView label;
		private LinearLayout icons;
		
		public ActionBar(Context ctx){
			super(ctx);
			setBackgroundColor(TEAL_LIGHT);
			setLayoutParams(new LinearLayout.LayoutParams(-1,dpInt(56)));
			label = new TextView(ctx);
			label.setTextColor(WHITE);
			LayoutParams lp = new LayoutParams(-1,-1,1);
			lp.leftMargin = dpInt(16);
			label.setLayoutParams(lp);
			label.setGravity(Gravity.CENTER_VERTICAL);
			label.setTypeface(Typeface.DEFAULT_BOLD);
			label.setTextSize(dp(10));
			addView(label);
			icons = new LinearLayout(ctx);
			icons.setLayoutParams(new LayoutParams(-2,-1,0));
			icons.setGravity(Gravity.CENTER);
			addView(icons);
		}
		
		public TextView getLabelView(){
			return label;
		}

		public void addIcon(Drawable image){
			addIcon(image,null);
		}

		public void addIcon(Drawable image,OnClickListener ocl){
			ImageView iv = new ImageView(getContext());
			int p = dpInt(48);
			iv.setLayoutParams(new LayoutParams(p,p));
			iv.setImageDrawable(image);
			iv.setScaleType(ScaleType.FIT_CENTER);
			iv.setOnClickListener(ocl);
			iv.setTag(icons.getChildCount());
			icons.addView(iv);
		}
		
		public void setIconVisibility(int index, int visibility){
			icons.getChildAt(index).setVisibility(visibility);
		}
		
		public void setIconVisibility(View v, int visibility){
			setIconVisibility((int) v.getTag(),visibility);
		}
		
		public void removeIconAt(int index){
			icons.removeViewAt(index);
		}
		
		public void removeIcon(View v){
			removeIconAt((int) v.getTag());
		}
		
	}
	
}
