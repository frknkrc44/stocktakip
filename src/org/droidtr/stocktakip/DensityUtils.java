package org.droidtr.stocktakip;
import android.content.res.*;

public class DensityUtils {
	
	public static float dp(float px){
		return Resources.getSystem().getDisplayMetrics().density * px;
	}
	
	public static int dpInt(float px){
		return (int) dp(px);
	}
	
}
